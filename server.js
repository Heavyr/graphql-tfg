var express = require('express');
var graphqlHTTP = require('express-graphql');
const schema = require('./Schemas/Schema.js')
const {graphql} = require('graphql');



let port = 3000;
const app = express();
app.use('/graphiql', graphqlHTTP({
  schema: schema,
  graphiql: true //set to false if you don't want graphiql enabled
}));

app.listen(port);
console.log('GraphQL API server running at localhost:'+ port);
