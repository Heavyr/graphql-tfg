FROM node:carbon

WORKDIR /usr/app

COPY package*.json ./
RUN npm install
RUN npm install graphql --save
RUN npm install express express-graphql graphql --save
RUN npm install --save request
RUN npm install --save request-promise
COPY . .

EXPOSE 49160

CMD [ "npm", "start" ]