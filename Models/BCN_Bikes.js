let { 
    GraphQLString,
    GraphQLInt, //distances 
    GraphQLObjectType, 
    GraphQLBoolean, 
    GraphQLFloat 
   } = require('graphql');
   
   const BusType = new GraphQLObjectType({
       name: 'BicisBcn', 
       description: 'Tipos de datos -> Bicicletas de Barcelona', 
       fields: () => ({  
           Bici:{
               name:'type',
               type:GraphQLString,
               description:'Tipo de Bicicletas',
               resolve(obj) {
                   if (obj['type']==="BIKE")
                        return "Bicicleta Normal"
                    else
                        return "Bicicleta Electrica"
               }
           },
           Calle:{
               name:'streetName',
               type:GraphQLString,
               description:'Calle donde está la estación de Bicicletas',
               resolve(obj) {
                 return obj['streetName']+" "+obj['streetNumber']
               }
           },
           Longitud:{
               name:'longitude',
               type:GraphQLFloat,
               description:'Longitud en coordenadas geográficas',
               resolve(obj) {
                 return obj['longitude']
               }
           },
           Latitud:{
               name:'latitude',
               type:GraphQLFloat,
               description:'Latitud en coordenadas geográficas',
               resolve(obj) {
                 return obj['latitude']
               }
           },
           NumeroBicis:{
               name:'bikes',
               type:GraphQLString,
               description:'Numero de Bicicletas de la estación',
               resolve(obj) {
                 return obj['bikes']
               }
           },
           Espacio:{
            name:'slots',
            type:GraphQLString,
            description:'Espacio disponible en la Estación de Bicicletas',
            resolve(obj) {
              return obj['slots']
            }
           },
           Cercanas:{
            name:'nearbyStations',
            type:GraphQLString,
            description:'Estaciones de Bicicletas cercanas',
            resolve(obj) {
              return obj['nearbyStations']
            }
           },
           Estado:{
            name:'status',
            type:GraphQLString,
            description:'Estaciones de Bicicletas cercanas',
            resolve(obj) {
                if (obj['status'] ==="OPN")
                    return "Disponible"
                else
                    return "Fuera de Servicio   "
            }
           }
      })
   })
   module.exports= BusType;