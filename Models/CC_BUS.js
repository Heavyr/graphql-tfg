let { 
 GraphQLString,
 GraphQLInt, //distances 
 GraphQLObjectType, 
 GraphQLBoolean, 
 GraphQLFloat 
} = require('graphql');

const BusType = new GraphQLObjectType({
	name: 'Caceres', 
	description: 'Tipos de datos -> Autobuses de Cáceres', 
	fields: () => ({  
		Bus:{
      name:'gtfs_headsign.value',
      type:GraphQLString,
      description:'Letrero del Autobus',
      resolve(obj) {
        return obj.gtfs_headsign.value
      }
    },
		Parada:{
      name:'foaf_name.value',
      type:GraphQLString,
      description:'Parada del Autobus',
      resolve(obj) {
        return obj.foaf_name.value
      }
    },
		Longitud:{
      name:'geo_long.value',
      type:GraphQLFloat,
      description:'Longitud en coordenadas geográficas',
      resolve(obj) {
        return obj.geo_long.value
      }
    },
		Direccion:{
      name:'gtfs_direction.value',
      type:GraphQLString,
      description:'Dirección que lleva el Autobus',
      resolve(obj) {
      if (obj.gtfs_direction.value==true)
        return 'Ida'
      else
        return 'Vuelta'
      }
    },
		Latitud:{
      name:'geo_lat.value',
      type:GraphQLFloat,
      description:'Latitud en coordenadas geográficas',
      resolve(obj) {
        return obj.geo_lat.value
      }
    }
   })
})
module.exports= BusType;