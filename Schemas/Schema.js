let {
  GraphQLList,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
} = require('graphql');

const _ = require('lodash'); 
const rp = require('request-promise');
const BCN_BUS = require ('../Models/BCN_BUS.js');
const CC_BUS = require ('../Models/CC_BUS.js');
const Malaga_BUS = require ('../Models/Malaga_BUS.js');
const Santander_BUS = require ('../Models/Santander_BUS.js');
const Barcelona_Bikes = require ('../Models/BCN_Bikes.js');

const BusQuery = new GraphQLObjectType({
    name: 'Root_Query',
    description: 'query',
	fields: () =>({
		Barcelona:{
			type: new GraphQLList(BCN_BUS),
			args:{
				limit:{type:GraphQLInt},
				Bus:{type:GraphQLString},
				Parada:{type:GraphQLString},
				Longitud:{type:GraphQLFloat},
				Latitud:{type:GraphQLFloat}
 			},
 			resolve: (_,args) => rp({
 				uri: `http://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search`,
 				qs:{
					resource_id: '2d190658-93ac-4c43-a23f-c5d313b1ae9c'
				}
			}).then(function(responseJson) {
				var parsed = JSON.parse(responseJson);
				var results = parsed.result.records;
				var response=[];
				var bool=false;
				if (typeof args.Bus !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].EQUIPAMENT == args.Bus)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].EQUIPAMENT == args.Bus)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Parada !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].NOM_DISTRICTE == args.Parada)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].NOM_DISTRICTE == args.Parada)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Longitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].LONGITUD == args.Longitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].LONGITUD == args.Longitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Latitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].LATITUD == args.Latitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].LATITUD == args.Latitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if(typeof args.limit !== "undefined"){
					bool=true;
					if(response.length>0){
						results=[];
						if (response.length>args.limit){
							for (i = 0; i < args.limit; i++)
								results=results.concat(response[i]);
							response=results;
						}
					}else{
						if (results.length>args.limit)
							for (i = 0; i < args.limit; i++)
								response=response.concat(results[i]);
						else
							response=results;
					}
				}
				if (bool)
					return response;
				else
					return results;
			})
		},
		Bicicletas_Barcelona:{
			type: new GraphQLList(Barcelona_Bikes),
			args:{
				limit:{type:GraphQLInt},
				Bici:{type:GraphQLString},
				Calle:{type:GraphQLString},
				Longitud:{type:GraphQLFloat},
				Latitud:{type:GraphQLFloat},
				NumeroBicis:{type:GraphQLString},
				Espacio:{type:GraphQLString},
				Cercanas:{type:GraphQLString},
				Estado:{type:GraphQLString},
			},
			resolve: (_,args) => rp({
				uri: `http://wservice.viabicing.cat/v2/stations`,
			}).then(function(responseJson) {
				var parsed = JSON.parse(responseJson);
				var results = parsed.stations;
				var response=[];
				var bool=false;
				if (typeof args.Bici !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['type'] == args.Bici)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['type'] == args.Bici)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Calle !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['streetName'] == args.Calle)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['streetName'] == args.Calle)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Longitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['longitude'] == args.Longitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['longitude'] == args.Longitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Cercanas !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['nearbyStations'] == args.Cercanas)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['nearbyStations'] == args.Cercanas)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Estado !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['status'] == args.Estado)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['status'] == args.Estado)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Espacio !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['slots'] == args.Espacio)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['slots'] == args.Espacio)
								response=response.concat(results[i]);
						results=response;
					}
				}

				if (typeof args.NumeroBicis !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['bikes'] == args.NumeroBicis)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['bikes'] == args.NumeroBicis)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Latitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['latitude'] == args.Latitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['latitude'] == args.Latitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if(typeof args.limit !== "undefined"){
					bool=true;
					if(response.length>0){
						results=[];
						if (response.length>args.limit){
							for (i = 0; i < args.limit; i++)
								results=results.concat(response[i]);
							response=results;
						}
					}else{
						if (results.length>args.limit)
							for (i = 0; i < args.limit; i++)
								response=response.concat(results[i]);
						else
							response=results;
					}
				}
				if (bool)
					return response;
				else
					return results;
			})
		},
		Caceres:{
			type: new GraphQLList(CC_BUS),
			args:{
				limit:{type:GraphQLInt},
				Bus:{type:GraphQLString},
				Parada:{type:GraphQLString},
				Longitud:{type:GraphQLFloat},
				Direccion:{type:GraphQLString},
				Latitud:{type:GraphQLFloat}
 			},
 			resolve: (_,args) => rp({
 				uri: `http://opendata.caceres.es/sparql/`,
 				qs:{
					query: 'SELECT ?geo_long ?geo_lat ?foaf_name ?gtfs_headsign ?gtfs_direction WHERE {?uri geo:long ?geo_long. ?uri geo:lat ?geo_lat. ?uri foaf:name ?foaf_name. ?URI gtfs:headsign ?gtfs_headsign . ?URI gtfs:direction ?gtfs_direction .}',
					format:'json',
				}
			}).then(function(responseJson) {
				var parsed = JSON.parse(responseJson);
				var results = parsed.results.bindings;
				var response=[];
				var bool=false;
				if (typeof args.Bus !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].gtfs_headsign.value == args.Bus)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].gtfs_headsign.value == args.Bus)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Parada !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].foaf_name.value == args.Parada)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].foaf_name.value == args.Parada)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Longitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].geo_long.value == args.Longitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].geo_long.value == args.Longitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Direccion !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].gtfs_direction.value == true){
								if (args.Direccion == 'Ida')
									results=results.concat(response[i]);
							}
							else if (args.Direccion == 'Vuelta')
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].gtfs_direction.value == true){
								if (args.Direccion == 'Ida')
									response=response.concat(results[i]);
								}
							else if (args.Direccion == 'Vuelta')
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Latitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].geo_lat.value == args.Latitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].geo_lat.value == args.Latitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if(typeof args.limit !== "undefined"){
					bool=true;
					if(response.length>0){
						results=[];
						if (response.length>args.limit){
							for (i = 0; i < args.limit; i++)
								results=results.concat(response[i]);
							response=results;
						}
					}else{
						if (results.length>args.limit)
							for (i = 0; i < args.limit; i++)
								response=response.concat(results[i]);
						else
							response=results;
					}
				}
				if (bool)
					return response;
				else
					return results;
			})
		},
		Malaga:{
			type: new GraphQLList(Malaga_BUS),
			args:{
				limit:{type:GraphQLInt},
				Bus:{type:GraphQLString},
				Parada:{type:GraphQLString},
				Longitud:{type:GraphQLFloat},
				Direccion:{type:GraphQLString},
				Latitud:{type:GraphQLFloat}
 			},
 			resolve: (_,args) => rp({
 				uri: `http://datosabiertos.malaga.eu/api/3/action/datastore_search`,
 				qs:{
					resource_id: 'd7eb3174-dcfb-4917-9876-c0e21dd810e3'
				}
			}).then(function(responseJson) {
				var parsed = JSON.parse(responseJson);
				var results = parsed.result.records;
				var response=[];
				var bool=false;
				if (typeof args.Bus !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].nombreLinea == args.Bus)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].nombreLinea == args.Bus)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Parada !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].nombreParada == args.Parada)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].nombreParada == args.Parada)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Longitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].lon == args.Longitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].lon == args.Longitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Direccion !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].sentido == true){
								if (args.Direccion == response[i].cabeceraIda)
									results=results.concat(response[i]);
							}
							else if (args.Direccion == response[i].cabeceraVuelta)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].sentido == true){
								if (args.Direccion == results[i].cabeceraIda)
									response=response.concat(results[i]);
								}
							else if (args.Direccion == results[i].cabeceraVuelta)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Latitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i].lat == args.Latitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i].lat == args.Latitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if(typeof args.limit !== "undefined"){
					bool=true;
					if(response.length>0){
						results=[];
						if (response.length>args.limit){
							for (i = 0; i < args.limit; i++)
								results=results.concat(response[i]);
							response=results;
						}
					}else{
						if (results.length>args.limit)
							for (i = 0; i < args.limit; i++)
								response=response.concat(results[i]);
						else
							response=results;
					}
				}
				if (bool)
					return response;
				else
					return results;
			})
		},
		Santander:{
			type: new GraphQLList(Santander_BUS),
			args:{
				limit:{type:GraphQLInt},
				Bus:{type:GraphQLString},
				Parada:{type:GraphQLString},
				Longitud:{type:GraphQLFloat},
				Direccion:{type:GraphQLString},
				Latitud:{type:GraphQLFloat}
 			},
 			resolve: (_,args) => rp({
 				uri: `http://datos.santander.es/api/rest/datasets/paradas_bus.json`,
			}).then(function(responseJson) {
				var parsed = JSON.parse(responseJson);
				var results = parsed.resources;
				var response=[];
				var bool=false;
				if (typeof args.Bus !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['ayto:numero'] == args.Bus)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['ayto:numero'] == args.Bus)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Parada !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['ayto:parada'] == args.Parada)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['ayto:parada'] == args.Parada)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Longitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['wgs84_pos:long'] == args.Longitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['wgs84_pos:long'] == args.Longitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Direccion !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['ayto:sentido'] == args.Direccion)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['ayto:sentido'] == args.Direccion)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if (typeof args.Latitud !== "undefined"){
					bool=true;
					if (response.length>0){
						results=[];
						for (i = 0; i < response.length; i++)
							if (response[i]['wgs84_pos:lat'] == args.Latitud)
								results=results.concat(response[i]);
						response=results;
					}else{
						for (i = 0; i < results.length; i++)
							if (results[i]['wgs84_pos:lat'] == args.Latitud)
								response=response.concat(results[i]);
						results=response;
					}
				}
				if(typeof args.limit !== "undefined"){
					bool=true;
					if(response.length>0){
						results=[];
						if (response.length>args.limit){
							for (i = 0; i < args.limit; i++)
								results=results.concat(response[i]);
							response=results;
						}
					}else{
						if (results.length>args.limit)
							for (i = 0; i < args.limit; i++)
								response=response.concat(results[i]);
						else
							response=results;
					}
				}
				if (bool)
					return response;
				else
					return results;
			})
		}
	})
})
const Bus_Schema = new GraphQLSchema({
	query: BusQuery
});
module.exports= Bus_Schema;
